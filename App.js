/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  NativeModules
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

// import ToastExample from './ToastExample';
console.log('NativeModules', NativeModules);
const myModule = NativeModules.ToastExample;

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: null
    };
  }
  async componentDidMount() {
    try {
      let response = await fetch(
        'https://prestage.linx.safemobile.com/api/generate-token',
      );
      let responseJson = await response.json();
      console.log('token', responseJson.data.token)
      this.setState({ token: responseJson.data.token });
    } catch (error) {
      console.error(error);
    }
  }
  async mumbleTest() {
    try {
      console.log('mumbleTest');
      // console.log('ToastExample 1111', ToastExample)
      ToastExample.show('Awesome', ToastExample.SHORT);
    } catch (error) {
      console.error(error);
    }
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View>
          <Text>
            {this.state.token}
          </Text>
        </View>
        <View style={styles.button}>
          {/* <Button
            style={styles.authBtn}
            title="Mumble"
            onPress={() => this.mumbleTest()}
          /> */}
                <Button title = "Test Native Bridge" onPress = {()=>{
        myModule.show("test", (fromJavaCode) => {
        
        });
      }}></Button>
        </View>
      </SafeAreaView>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
    marginHorizontal: 10,
  },
  button: {
    margin: 50,
    flexDirection: "column",
    justifyContent: "space-between",
    justifyContent: "center",
    backgroundColor: "red"
  },
  token: {
    textAlign: "center",
    marginVertical: 8,
  },
  authBtn: {
    paddingTop: 30
  },
});